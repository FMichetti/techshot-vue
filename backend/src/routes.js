const express = require("express");


const ProdutoController = require('./controllers/ProdutoController');


const routes = express.Router();


routes.get('/produtos', ProdutoController.listar);
routes.get("/produtos/:id", ProdutoController.listarUmProduto);
routes.post("/produtos", ProdutoController.create);
routes.put("/produtos/:id", ProdutoController.editar);
routes.delete("/produtos/:id", ProdutoController.delete);



module.exports = routes;
