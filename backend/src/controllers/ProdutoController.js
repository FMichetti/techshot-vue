const connection = require("../database/connection");

module.exports = {
  async listar(request, response) {
    const produtos = await connection("produtos").select("*");

    if (!produtos || produtos.length === 0) {
      return response.status(400).json({ error: "Produtos nao encontrados." });
    }

    return response.json(produtos);
  },

  async listarUmProduto(request, response) {
    const { id } = request.params;

    const produtos = await connection("produtos")
      .where("id", id)
      .select("*")
      .first();

    if (!produtos) {
      return response.status(400).json({ error: "Produto nao encontrado." });
    }

    return response.json(produtos);
  },

  async create(request, response) {
    var {
      nome,
      preco
    } = request.body;

    await connection("produtos").insert({
      nome,
      preco
    });

    return response.status(200).send("Produto criado com sucesso!");
  },

  async editar(request, response) {
    var {
      nome,
      preco
    } = request.body;
    const { id } = request.params;

    const produto = await connection("produtos")
      .where("id", id)
      .select("*")
      .first();

    if (!produto) {
      return response.status(400).json({ error: "Produto nao encontrado." });
    }

    await connection("produtos").where("id", id).update({
      nome: nome,
      preco: preco, 
    });

    return response.status(200).send("Produto alterado com sucesso!");
  },

  async delete(request, response) {
    const { id } = request.params;

    const produto = await connection("produtos")
      .where("id", id)
      .select("*")
      .first();

    if (!produto) {
      return response.status(400).json({ error: "Produto nao encontrado." });
    }

    await connection("produtos").where("id", id).delete();

    return response.status(200).send("Produto deletado com sucesso!");
  },
};
