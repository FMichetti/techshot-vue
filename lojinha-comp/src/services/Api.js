import axios from "axios";

export const baseURL = "http://localhost:3333";
export default () => {
  return axios.create({
    baseURL: baseURL
  });
};
