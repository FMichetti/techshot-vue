import Api from "./Api";

const baseUrl = "/produtos"

const listarProdutos = () => {
  return Api().get(baseUrl);
};

const adicionarProdutos = (formData) => {
    const form = new FormData();
    form.append("nome",formData.nome);
    form.append("preco",formData.preco);

    return Api().post(baseUrl, formData);
}

export { listarProdutos, adicionarProdutos };
