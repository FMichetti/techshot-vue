import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
    mode:'history',
    base: process.env.BASE_URL,
    routes:[
        {
            path: '/',
            name: 'home',
            component: () => import("@/views/Home.vue"),
            children:[
                {
                    path: '/produtos',
                    name: 'produtos',
                    component: () => import("@/views/Produtos.vue"),
                },
                {
                    path: '/estoque',
                    name: 'estoque',
                    component: () => import("@/views/EstoqueProdutos.vue"),
                }
            ]
        },
        
    ]
})

export default router
